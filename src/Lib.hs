module Lib
    --( someFunc
    --) where
where
import Props
import Data.List (transpose)
import Data.Maybe (isJust)
import Control.Lens
import Control.Monad
import Control.Monad.ST
import Control.Monad.Random.Lazy
import System.Random
import Data.Array.ST
import GHC.Arr
import Data.Foldable
import Control.Applicative
import Control.Monad
import Options.Applicative

data Side = Nun | One | Two deriving (Eq, Ord, Enum, Show)

data Square = Square Side Side Side Side deriving (Eq, Ord, Show)

printSquare :: Square -> Maybe Char
printSquare (Square Nun Nun Nun Nun) = Just ' '
printSquare (Square One Nun One Nun) = Just '│'
printSquare (Square One Nun One One) = Just '┤'
printSquare (Square One Nun One Two) = Just '╡'
printSquare (Square Two Nun Two One) = Just '╢'
printSquare (Square Nun Nun Two One) = Just '╖' 
printSquare (Square Nun Nun One Two) = Just '╕'
printSquare (Square Two Nun Two Two) = Just '╣'
printSquare (Square Two Nun Two Nun) = Just '║'
printSquare (Square Nun Nun Two Two) = Just '╗'
printSquare (Square Two Nun Nun Two) = Just '╝'
printSquare (Square Two Nun Nun One) = Just '╜'
printSquare (Square One Nun Nun Two) = Just '╛'
printSquare (Square Nun Nun One One) = Just '┐'
printSquare (Square One One Nun Nun) = Just '└'
printSquare (Square One One Nun One) = Just '┴'
printSquare (Square Nun One One One) = Just '┬'
printSquare (Square One One One Nun) = Just '├'
printSquare (Square Nun One Nun One) = Just '─'
printSquare (Square One One One One) = Just '┼'
printSquare (Square One Two One Nun) = Just '╞'
printSquare (Square Two One Two Nun) = Just '╟'
printSquare (Square Two Two Nun Nun) = Just '╚'
printSquare (Square Nun Two Two Nun) = Just '╔'
printSquare (Square Two Two Nun Two) = Just '╩'
printSquare (Square Nun Two Two Two) = Just '╦'
printSquare (Square Two Two Two Nun) = Just '╠'
printSquare (Square Nun Two Nun Two) = Just '═'
printSquare (Square Two Two Two Two) = Just '╬'
printSquare (Square One Two Nun Two) = Just '╧'
printSquare (Square Two One Nun One) = Just '╨'
printSquare (Square Nun Two One Two) = Just '╤'
printSquare (Square Nun One Two One) = Just '╥'
printSquare (Square Two One Nun Nun) = Just '╙'
printSquare (Square One Two Nun Nun) = Just '╘'
printSquare (Square Nun Two One Nun) = Just '╒'
printSquare (Square Nun One Two Nun) = Just '╓'
printSquare (Square Two One Two One) = Just '╫'
printSquare (Square One Two One Two) = Just '╪'
printSquare (Square One Nun Nun One) = Just '┘'
printSquare (Square Nun One One Nun) = Just '┌'
printSquare _ = Nothing

type Grid = [[Square]]

allSquares :: [Square] 
allSquares = filter (isJust . printSquare) $  Square <$> side <*> side <*> side <*> side
  where
    side = enumFrom Nun

grid :: Int -> Int -> [[[Square]]]
grid w h = replicate h $ replicate w allSquares

require' :: (a -> b -> Bool) -> PVar [] a -> PVar [] b -> Prop ()
require' f a b = constrain a b disj
  where 
    disj a bs = filter (f a) bs

requireBoth :: (a -> b -> Bool) -> PVar [] a -> PVar [] b -> Prop ()
requireBoth f a b = do
    require' f a b
    require' (flip f) b a

constrainHorizontal :: PVar [] Square -> PVar [] Square -> Prop ()
constrainHorizontal = requireBoth (\(Square _ r _ _) (Square _ _ _ l) -> l == r)

constrainVertical :: PVar [] Square -> PVar [] Square -> Prop ()
constrainVertical = requireBoth (\(Square _ _ b _) (Square t _ _ _) -> t == b)

neighbours :: [a] -> [(a, a)]
neighbours (x1:x2:xs) = (x1, x2):(neighbours (x2:xs))
neighbours _ = []

linkCells :: [[PVar [] Square]] -> Prop ()
linkCells cells = do
    for_ cells $ \row -> 
      for_ (neighbours row) $ \(f, s) -> constrainHorizontal f s
    for_ (transpose cells) $ \col -> 
      for_ (neighbours col) $ \(f, s) -> constrainVertical f s

constrainGrid :: [[[Square]]] -> Prop [[PVar [] Square]]
constrainGrid board = do
  squares <- (traverse.traverse) newPVar board
  linkCells squares
  return squares

printGrid :: [[Square]] -> String
printGrid g = case ((traverse.traverse) printSquare g) of
                (Just l) -> unlines $ l
                Nothing -> "Ooops"

setBorder :: [[[Square]]] -> [[[Square]]]
setBorder g = g 
   & over (traverse . _head) (filter (\(Square _ _ _ l) -> l == Nun)) 
   & over (_head . traverse) (filter (\(Square t _ _ _) -> t == Nun)) 
   & over (traverse . _last) (filter (\(Square _ r _ _) -> r == Nun)) 
   & over (_last . traverse) (filter (\(Square _ _ b _) -> b == Nun)) 
 
shuffle :: RandomGen g => [a] -> Rand g [a]
shuffle xs = do
    let l = length xs
    rands <- forM [0..(l-2)] $ \i -> getRandomR (i, l-1)
    let ar = runSTArray $ do
          ar' <- thawSTArray $ listArray (0, l-1) xs
          forM_ (zip [0..] rands) $ \(i, j) -> do
            vi <- readSTArray ar' i
            vj <- readSTArray ar' j
            writeSTArray ar' j vi
            writeSTArray ar' i vj
          return ar'
    return (elems ar)

opts :: Parser (IO ())
opts = run <$>
    (option auto
        (  long "width"
        <> short 'w'
        <> metavar "NUMBER"
        <> help "width of the grid"
        <> value 10
        <> showDefault
    )) <*>
    (option auto
        (  long "height"
        <> short 'd'
        <> metavar "NUMBER"
        <> help "height of the grid"
        <> value 10
        <> showDefault
    )) <*>
    filterOpts

filterOpts :: Parser (Square -> Bool)
filterOpts = 
   ((incs <$> (strOption
          ( long "include"
         <> metavar "SQUARES"
         <> help "limit shapes to a restricted set" )
     )
    ) <|>
    (excs <$> (strOption
          ( long "exclude"
         <> metavar "SQUARES"
         <> help "restrict shapes to a limited set" )
     )
    )) <|>
    (pure (pure True))
  where
    incs :: String -> Square -> Bool
    incs chars sqr = elem (printSquare sqr) (Just <$> chars) 
    excs :: String -> Square -> Bool
    excs chars sqr = not $ incs chars sqr

run :: Int -> Int -> (Square -> Bool) -> IO  ()
run w h pred = do
    let g = grid w h
    theGrid <- evalRandIO $ g 
         & setBorder
         & (fmap.fmap) (filter pred)
         & (traverse.traverse) shuffle
    let results = solve (fmap.fmap) $ constrainGrid $ theGrid
    case results of
        (Just r) -> putStrLn $ printGrid r
        Nothing -> putStrLn "No Solutions"

someFunc :: IO ()
someFunc = join . customExecParser (prefs showHelpOnError) $
  info (helper <*> opts)
  (  fullDesc
  <> header "Wavefunction Collapse"
  <> progDesc ("An experiment with mad-props\nPossible Squares Are\n" ++ (printGrid [allSquares]))
  )
